@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Supplier Dashboard
                </div>


                <div class="panel-body">
                    You are logged in as <strong>Supplier</strong>!
                </div>

                <div class="services">
                    <div class="container">
                        <div class="col-md-8">
                            <div class="option">
                                <h3>Choose option</h3>
                                <div class="title-border"></div>
                            </div>
                            <a href="supplier_input.html" class="btn btn-success button_margin">Input</a>
                            <a href="supplier_produce.html" class="btn btn-success button_margin">Produce</a>
                            <a href="supplier_service.html" class="btn btn-success button_margin">Service</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>





<br>

<div class="footer">
    <div class="container">
        <div class="col-md-6 pull-left">
            Copyright &copy; ২০১৭ mPrice | All Rights Reserved.
        </div>
        <div class="col-md-6 pull-right">
            <p>Developed by <a href="https://ulab.edu.bd/">ULAB</a>. Powered by <a href="http://www.biid.org.bd/">BIID</a></p>
        </div>
    </div>
</div>
@endsection