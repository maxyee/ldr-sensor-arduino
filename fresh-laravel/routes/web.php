<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

 
 
Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index');

// Lets make Admin Route Group
Route::prefix('admin')->group(function() {
  Route::get('/login', 'Auth\AdminLoginController@showLoginForm')->name('admin.login');
  Route::post('/login', 'Auth\AdminLoginController@login')->name('admin.login.submit');
  Route::get('/', 'AdminController@index')->name('admin.dashboard');
});

//Supplier routes
//Route::get('/supplier', 'SupplierController@index');
//Routes for supplier Login....
Route::prefix('supplier')->group(function() {
  Route::get('/login', 'Auth\SupplierLoginController@showLoginForm')->name('supplier.login');
  Route::post('/login', 'Auth\SupplierLoginController@login')->name('supplier.login.submit');
  Route::get('/', 'SupplierController@index')->name('supplier.dashboard');
});

//supplier Register Route:--
Route::prefix('supplier')->group(function (){
   Route::get('/register', 'Auth\SupplierRegisterController@showRegisterForm')->name('supplier.register');
   Route::post('/register', 'Auth\SupplierRegisterController@register')->name('supplier.register.submit');
   //Route::get('/','SupplierRegisterController@index')->name('supplier.dashboard');
});

//Farmer Login Route:----
Route::prefix('farmer')->group(function() {
  Route::get('/login', 'Auth\FarmerLoginController@showLoginForm')->name('farmer.login');
  Route::post('/login', 'Auth\FarmerLoginController@login')->name('farmer.login.submit');
  Route::get('/', 'FarmerController@index')->name('farmer.dashboard');
});

//Farmer Register Route:----
Route::prefix('farmer')->group(function (){
   Route::get('/register', 'Auth\FarmerRegisterController@showRegisterForm')->name('farmer.register');
   Route::post('/register', 'Auth\FarmerRegisterController@register')->name('farmer.register.submit');
   //Route::get('/','SupplierRegisterController@index')->name('supplier.dashboard');
});