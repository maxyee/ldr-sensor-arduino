var five = require('johnny-five');
var board = new five.Board();
     
	var pubnub = require('pubnub').init({
      subscribe_key: 'sub-c-84e70666-17f9-11e7-894d-0619f8945a4f',
      publish_key:   'pub-c-294c451e-79da-4fd0-8fc8-e94f86a21df4'
    });
	
	var channel = 'led';
 
board.on('ready', function() {
  var led = new five.Led(13); // pin 13

  pubnub.subscribe({
    channel: channel,
    message: function(m) {
      if(m.blink === true) {
        led.blink(500);
      } else {
        led.stop();
        led.off();
      }
    },
    error: function(err) {console.log(err);}
  });
  
});