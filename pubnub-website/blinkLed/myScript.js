var pubnub = PUBNUB.init({
    publish_key: 'pub-c-294c451e-79da-4fd0-8fc8-e94f86a21df4', // Use your pub key
    subscribe_key: 'sub-c-84e70666-17f9-11e7-894d-0619f8945a4f' // Use your sub key
});

// Use the same channel name
var channel = 'led';

var button = document.querySelector('button.on');

var blinkState = true;

/***
Subscribe data from all subscibers of the channel to set the button state correctly
***/
pubnub.subscribe({
    channel: channel,
    message: function(m) {
        blinkState = m.blink; // the raw data
        blinkState = !blinkState; // toggle it to lable the button
        button.textContent = (blinkState) ? 'Blink LED' : 'Stop LED';
        console.log(blinkState);
    }
});

/*
Upon a button click, publish the data.
Arduino will subscribe it and blink LED
*/
button.addEventListener('click', function(e) {
    pubnub.publish({
        channel: channel,
        message: { blink: blinkState },
        callback: function(m) {
            console.log(m);
        }
    });

});